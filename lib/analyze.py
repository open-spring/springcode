from openai.gpt import GPT, GPTSimpleVectorIndex

def analyze_code_with_gpt(index_file_name):

    index = GPTSimpleVectorIndex.load_from_disk(index_file_name)

    # Perform GPT analysis
    prompt_summarize = f"Given the following source code and documentation:\n\n{training_data}\n\nPlease provide a summary of what the program does (max 400 characters)."
    summary = index.query(prompt_summarize).response[:400]

    prompt_dependencies = f"Given the following source code and documentation:\n\n{training_data}\n\nPlease list any external dependencies such as databases or APIs."
    dependencies = index.query(prompt_dependencies).response

    prompt_mermaid = (
        f"Given the following source code and documentation:\n\n"
        f"{training_data}\n\n"
        f"Please provide a description of the program architecture "
        f"and main components, in a format that can be translated "
        f"into a Mermaid diagram."
    )
    mermaid_description = index.query(prompt_mermaid).response

    prompt_performance = "Given the following source code and documentation, please suggest the top 5 performance improvements:"
    performance = index.query(prompt_performance).response

    prompt_security = "Given the following source code and documentation, please suggest the top 5 security improvements:"
    security = index.query(prompt_security).response

    prompt_containerization = "Given the following source code and documentation, please suggest the top 5 containerization improvements for Kubernetes:"
    containerization = index.query(prompt_containerization).response

    gpt_result = {
        "summary": summary,
        "dependencies": dependencies,
        "mermaid_description": mermaid_description,
        "performance": performance,
        "security": security,
        "containerization": containerization
    }

    return gpt_result