from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, PasswordField, SelectField, TextAreaField
from wtforms.validators import DataRequired
import os


class UseAppForm(FlaskForm):
    apps = []
    for file in os.listdir("./apps"):
        if file.endswith(".json"):
            size = os.path.getsize(f"./apps/{file}")
            date = os.path.getctime(f"./apps/{file}")
            date_str = datetime.datetime.fromtimestamp(date).strftime('%Y-%m-%d %H:%M:%S')
            apps.append((file, f"{date_str} - {size} bytes"))

    app = SelectField('Select App', choices=apps)