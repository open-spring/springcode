from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, PasswordField, SelectField, TextAreaField
from wtforms.validators import DataRequired

class AddGitRepo(FlaskForm):
    repo_url = StringField("Repository URL")
    username = StringField("Username (optional)", default="")
    password = StringField("Password (optional)", default="")
    file_suffix = SelectField("File Suffix", choices=[(".py", "Python"), (".java", "Java"), (".js", "JavaScript"), (".c", "C"), (".cpp", "C++"), (".rb", "Ruby"), (".php", "PHP"), (".ts", "TypeScript"), (".swift", "Swift"), (".go", "Go")], default=".py")
    submit = SubmitField("Analyze Code")