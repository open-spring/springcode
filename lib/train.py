import os
import tempfile
from git import Repo
from flask_socketio import SocketIO, emit


doc_suffixes = [".md", ".txt", ".doc"]

# Funktion zum Klonen des Git-Repositories
def clone_repository(repo_url, username, password, temp_dir):
    socketio.emit("progress", "Cloning repository...")

    # Clone the repository using the provided credentials
    repo = Repo.clone_from(repo_url, temp_dir)

    socketio.emit("progress", "Repository cloned.")
    return repo

# Funktion zum Extrahieren von Code und optionalen Dokumentationen
def extract_source_code_and_docs(repo, temp_dir, file_suffixes, docs_folder=None):
    socketio.emit("progress", "Extracting source code and documentation...")

    if docs_folder is None:
        docs_folder = temp_dir

    source_files = []
    docs_files = []
    num_files = 0
    files_processed = 0

    # Calculate the total number of files in the repository for progress calculation
    for root, _, files in os.walk(temp_dir):
        num_files += len(files)

    last_update_time = 0

    for root, _, files in os.walk(temp_dir):
        for file in files:
            file_path = os.path.join(root, file)

            # Extract source code files based on the given file_suffixes
            for file_suffix in file_suffixes:
                if file.endswith(file_suffix):
                    with open(file_path, "r", encoding="utf-8") as f:
                        content = f.read()
                    source_files.append((file, content))
                    break

            # Extract documentation files from the docs folder
            if root.startswith(docs_folder):
                for doc_suffix in doc_suffixes:
                    if file.endswith(doc_suffix):
                        with open(file_path, "r", encoding="utf-8") as f:
                            content = f.read()
                        docs_files.append((file, content))
                        break

            files_processed += 1

            # Update the progress every 200ms
            current_time = time.time()
            if current_time - last_update_time >= 0.2:
                progress = (files_processed / num_files) * 100
                socketio.emit("progress", f"Processing files: {progress:.2f}% completed")
                last_update_time = current_time

    socketio.emit("progress", "Source code and documentation extracted.")
    return code, docs

# Funktion zum Vorbereiten von Trainingsdaten
def create_gpt_training_index(source_files, docs_files):
    socketio.emit("progress", "Creating GPT-3.5 training index...")

    num_files = len(source_files) + len(docs_files)
    files_processed = 0
    last_update_time = 0

    training_data = []

    for file_name, content in source_files + docs_files:
        training_data.append({
            "file_name": file_name,
            "content": content
        })

        files_processed += 1

        # Update the progress every 200ms
        current_time = time.time()
        if current_time - last_update_time >= 0.2:
            progress = (files_processed / num_files) * 100
            socketio.emit("progress", f"Creating index: {progress:.2f}% completed")
            last_update_time = current_time

    # Save the training index to a JSON file
    with open("gpt_training_index.json", "w") as f:
        json.dump(training_data, f)

    socketio.emit("progress", "GPT-3.5 training index created.")

# Funktion zum Erstellen der Trainingsdaten
def construct_index(training_data, index_file, temperature=0.7, model_name="text-davinci-003", max_tokens=512):
    max_input_size = 4096
    num_outputs = max_tokens
    max_chunk_overlap = 20
    chunk_size_limit = 600

    prompt_helper = PromptHelper(max_input_size, num_outputs, max_chunk_overlap, chunk_size_limit=chunk_size_limit)

    llm_predictor = LLMPredictor(llm=OpenAI(temperature=temperature, model_name=model_name, max_tokens=num_outputs))

    documents = SimpleDirectoryReader(training_data).load_data()

    index = GPTSimpleVectorIndex(documents, llm_predictor=llm_predictor, prompt_helper=prompt_helper)

    num_documents = len(documents)
    for i, document in enumerate(documents):
        index.add_document(document)

        # Update progress every 200ms
        current_time = time.time()
        if current_time - last_update_time >= 0.2:
            progress = (i + 1) / num_documents * 100
            socketio.emit("progress", f"Creating index: {progress:.2f}% completed")
            last_update_time = current_time

    index.save_to_disk(index_file)

    socketio.emit("progress", "GPT-3.5 training index created.")
    return index

def train(repo_url, username, password, file_suffix, index_file_name):
    with tempfile.TemporaryDirectory() as temp_dir:
        # Clone the repository
        repo = clone_repository(repo_url, username, password, temp_dir)

        # Extract code and optional documentation
        code, docs = extract_source_code_and_docs(repo, temp_dir, file_suffixes, docs_folder)

        # Prepare the training data
        training_data = create_gpt_training_index(code, docs)

        # Create the training data
        index = construct_index(training_data, index_file_name)


    return true

