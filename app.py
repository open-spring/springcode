from flask import Flask, render_template, request, jsonify
from flask_socketio import SocketIO
import json
import os
import tempfile
from git import Repo
from gpt_index import SimpleDirectoryReader, GPTListIndex, GPTSimpleVectorIndex, LLMPredictor, PromptHelper
from langchain import OpenAI
from lib.form.addapp import AddGitRepo
from lib.form.useapp import UseAppForm
from lib.train import train
from time import sleep
from threading import Thread

app = Flask(__name__)
app.config['SECRET_KEY'] = 'any secret string'

# Create a SocketIO instance
socketio = SocketIO(app)

@app.route('/', methods=['GET', 'POST'])
def home():
    form = UseAppForm()
    if form.validate_on_submit():
        selected_app = form.app.data
        return redirect(url_for('useapp', app=selected_app))
    return render_template('home.html', form=form)

@app.route('/useapp/<model>')
def useapp(model):
    return render_template('result.html', app=model)


@app.route("/add", methods=["GET", "POST"])
def add():
    form = AddGitRepo(request.form)
    if form.validate_on_submit():
        repo_url = form.repo_url.data
        username = form.username.data
        password = form.password.data
        file_suffix = form.file_suffix.data

        # Call the train function in a separate thread
        thread = Thread(target=train, args=(repo_url, username, password, file_suffix))
        thread.start()

        return render_template("processing.html")
    else:
        return render_template("add.html", form=form)


@app.route('/results', methods=['POST'])
def results():
    gpt_result = analyze_code_with_gpt(repo_url, username, password, file_suffixes, index_file_name)
    
    return render_template('results.html', gpt_result=gpt_result)

if __name__ == "__main__":
    app.run(debug=True)

